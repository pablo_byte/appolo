-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-09-2015 a las 03:55:18
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `appolo`
--
CREATE DATABASE IF NOT EXISTS `appolo` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `appolo`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `farmacos`
--

DROP TABLE IF EXISTS `farmacos`;
CREATE TABLE IF NOT EXISTS `farmacos` (
  `id_farmaco` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `codigo_farmaco` varchar(255) COLLATE utf8_bin NOT NULL,
  `nombre_farmaco` varchar(255) COLLATE utf8_bin NOT NULL,
  `tipo_farmaco` varchar(255) COLLATE utf8_bin NOT NULL,
  `estado_farmaco` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_farmaco`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `farmacos`
--

INSERT INTO `farmacos` (`id_farmaco`, `codigo_farmaco`, `nombre_farmaco`, `tipo_farmaco`, `estado_farmaco`) VALUES
(1, '00000001', 'Aspirina', 'Comprimidos', 0),
(2, '00000002', 'Jarabe', 'Oral', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'medico', 'Medicos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login_attempts`
--

DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recetas`
--

DROP TABLE IF EXISTS `recetas`;
CREATE TABLE IF NOT EXISTS `recetas` (
  `id_receta` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_receta` int(11) NOT NULL,
  `rut_doctor` varchar(20) COLLATE utf8_bin NOT NULL,
  `rut_paciente` varchar(20) COLLATE utf8_bin NOT NULL,
  `nombre_paciente` varchar(100) COLLATE utf8_bin NOT NULL,
  `edad_paciente` int(11) NOT NULL,
  `direccion_paciente` varchar(255) COLLATE utf8_bin NOT NULL,
  `detalle_receta` text COLLATE utf8_bin NOT NULL,
  `estado_receta` int(11) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id_receta`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `recetas`
--

INSERT INTO `recetas` (`id_receta`, `tipo_receta`, `rut_doctor`, `rut_paciente`, `nombre_paciente`, `edad_paciente`, `direccion_paciente`, `detalle_receta`, `estado_receta`, `created`, `updated`) VALUES
(1, 1, '12345678-9', '98765432-1', 'Paciente 1', 20, 'Calle 1 n°2', '', 1, '2015-04-06 14:32:21', '0000-00-00 00:00:00'),
(2, 2, '99999999-9', '11111111-1', 'Paciente 2', 30, 'Calle 3 n°4', '', 1, '2015-04-07 03:10:15', '0000-00-00 00:00:00'),
(4, 2, '13475388-9', '15384845-9', 'Leslie', 31, 'rio corrrientes 525', '500 mg de abrazos', 1, '2015-04-27 02:01:44', '0000-00-00 00:00:00'),
(5, 1, '12345678-9', '99966548-9', 'Pepe', 21, 'calle 2', '<p>Radfiografias:</p>\r\n\r\n<ul>\r\n <li>Pelvis</li>\r\n <li>Torax</li>\r\n</ul>\r\n', 1, '2015-04-27 03:59:48', '0000-00-00 00:00:00'),
(6, 2, '12345678-9', '66-6', 'lalo', 23, 'calle 2', '<ol>\r\n <li>aspirinas</li>\r\n <li>viagra</li>\r\n <li>paracetamol</li>\r\n</ol>\r\n', 1, '2015-04-27 04:01:40', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `receta_items`
--

DROP TABLE IF EXISTS `receta_items`;
CREATE TABLE IF NOT EXISTS `receta_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_receta` int(11) NOT NULL,
  `id_farmaco` int(11) NOT NULL,
  `dosis` varchar(255) COLLATE utf8_bin NOT NULL,
  `cant_total` varchar(255) COLLATE utf8_bin NOT NULL,
  `estado_item` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `receta_items`
--

INSERT INTO `receta_items` (`id`, `id_receta`, `id_farmaco`, `dosis`, `cant_total`, `estado_item`) VALUES
(1, 1, 1, '1 pastilla c/8 horas', '21 pastillas', 1),
(2, 2, 1, '1 pastilla cada dia', '10', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `rut` varchar(20) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `password` varchar(80) NOT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `tipo` int(3) DEFAULT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `second_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `rut`, `username`, `email`, `phone`, `first_name`, `last_name`, `password`, `active`, `tipo`, `ip_address`, `salt`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `second_name`, `company`, `birth_date`) VALUES
(1, '13475388-9', 'administrator', 'admin@admin.com', '12345', 'Admin', 'Appolo', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', 1, 1, '', '9462e8eee0', NULL, NULL, NULL, 'KJKEugtxpFZKK8CKz9tBVu', 1268889823, 1430352813, 'A', 'Administrador', '1969-12-31'),
(2, '12345678-9', 'medico', 'medico@medico.cl', '50506060', 'Medico', 'General', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', 1, 2, '', NULL, NULL, NULL, NULL, NULL, 0, 1430357924, NULL, NULL, NULL),
(3, '99999999-9', 'House', 'house@house.cl', '70708080', 'Doctor', 'House', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', 1, 2, '', NULL, NULL, NULL, NULL, NULL, 0, 1430083178, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `via_administracion`
--

DROP TABLE IF EXISTS `via_administracion`;
CREATE TABLE IF NOT EXISTS `via_administracion` (
  `id_via` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre_via` varchar(255) COLLATE utf8_bin NOT NULL,
  `descripcion_via` text COLLATE utf8_bin NOT NULL,
  `estado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_via`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `via_administracion`
--

INSERT INTO `via_administracion` (`id_via`, `nombre_via`, `descripcion_via`, `estado`) VALUES
(1, 'Oral', '', 1),
(2, 'Sublingual', '', 0),
(3, 'Parenteral', '', 1),
(4, 'Rectal', '', 1),
(5, 'Típica', '', 1),
(6, 'Percutánea', '', 1),
(7, 'Inhalada', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `via_secundaria`
--

DROP TABLE IF EXISTS `via_secundaria`;
CREATE TABLE IF NOT EXISTS `via_secundaria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_via` int(10) unsigned NOT NULL,
  `nombre_via_sec` varchar(255) COLLATE utf8_bin NOT NULL,
  `descripcion_via_sec` text COLLATE utf8_bin NOT NULL,
  `estado` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `via_secundaria`
--

INSERT INTO `via_secundaria` (`id`, `id_via`, `nombre_via_sec`, `descripcion_via_sec`, `estado`) VALUES
(1, 1, 'Gotas', '', 0),
(2, 1, 'Jarabes', '', 0),
(3, 1, 'Tizanas', '', 1),
(4, 1, 'Elixires', '', 0),
(5, 3, 'Vía Intravenosa', '', 0),
(6, 3, 'Via Intraarterial', '', 0),
(7, 4, 'Supositorios', '', 0),
(8, 4, 'Cápsulas rectales', '', 0),
(9, 5, 'Baños', '', 0),
(10, 5, 'Lociones', '', 0);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
