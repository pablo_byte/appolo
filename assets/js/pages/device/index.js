var oType;

$(function(){
    
    oType = $('#device-types-table').DataTable({
        dom : 'rtp',
        serverSide : true,
        proccesing : true,
        ajax : {
            url : 'device/device_types_table',
            type : 'POST'
        },
        columnDefs : [
            {orderable: false, targets : [3]}
        ]
        
    });
    
    
    $('#addNewType').click(function(){
        
        $('#addEditModal').html('Crear Tipo de Dispositivo');
        $('#saveType').val('add');
        $('#alertDiv').hide();
        $('#add-edit-modal').modal('toggle');
    });
    
    $('#addEditSubmit').click(function(){
        
        var data = {
            type_name : $('#type_name').val(),
            type_geo : $('#type_geo').val(),
            type_img : $('#type_img').val(),
            type_vid : $('#type_vid').val(),
            save_type : $('#saveType').val(),
            type_id : $('#type_id').val()
        };
        
        $.ajax({
            url : 'device/save_type',
            type : 'POST',
            data : data,
            success : function(r)
            {
                if(r.status == 'success')
                {
                    oType.ajax.reload();
                    $('#add-edit-modal').modal('toggle');
                }
                
                if(r.message != '')
                {
                    $('#alertMessage').html(r.message);
                    $('#alertDiv').show();
                }
            }
        });
        
    });
});

function edit_type_device(id)
{
    $('#addEditModal').html('Editar Tipo de Dispositivo');
    $('#saveType').val('edit');
    $('#alertDiv').hide();
    
    $.post('device/get_device_type/'+id,function(r){
        console.log(r);
        
        $('#type_name').val(r.name);
        $('#type_geo').val(r.georeference);
        $('#type_img').val(r.image);
        $('#type_vid').val(r.video);
        $('#type_id').val(r.id);
        
        $('#add-edit-modal').modal('show');
    });
}