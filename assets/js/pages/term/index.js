var oTerm;

$(function(){
    
    oTerm = $('#term-table').DataTable({
        dom : 'rtp',
        serverSide: true,
        processing: true,
        ajax: {
            url: 'term/term_table',
            type: 'POST'
        },
        columnDefs: [
            {orderable : false, targets: 4}
        ]
    });
    
    $('#confirmation-button').click(function(){
        var term_id = $('#confirmation-id').val();
        console.log(term_id);
        $.post('term/delete', {term_id: term_id, delete_term: 1}, function(data){
            if(data.type == 'success') 
            {
                $('#confirmation-modal').modal('hide');
                oTerm.ajax.reload();
            } else {
                alert(data.error);
            }
        });
    });
    
});

function delete_term(term_id)
{
    $.post('term/delete/',{term_id: term_id, delete_term: 0},function(data){
        $('#confirmation-title').html(data.term.title);
        $('#confirmation-id').val(data.term.id);
        $('#confirmation-modal').modal('show');
    });
}