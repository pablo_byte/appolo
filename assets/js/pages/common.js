$(function () {

    $('[data-toggle="ajaxModal"]').on('click', function (e) {

        $('#ajaxModal').remove();
        e.preventDefault();
        var $this = $(this),
                $remote = $this.data('remote'),
                $modal = $('<div class="modal" id="ajaxModal"><div class="modal-body"></div></div>');
        $('body').append($modal);
        $modal.modal({backdrop: 'static', keyboard: false});
        $modal.load($remote);
    })

    $('#confirmation-button').click(function () {
        var id = $('#confirmation-id').val();
        var action = $('#confirmation-action').val();
        var url = $('#confirmation-url').val();

        if (id != '' && action != '') {
            $.post(url, {id: id, action: action, status: 1}, function (data) {

                $('#confirmation-id').val('');
                $('#confirmation-action').val('');

                if (data.status == 'success') {
                    $(data.table).DataTable().ajax.reload();
                    $('#confirmation-success-modal .modal-body').html(data.message);

                } else {
                    $('#confirmation-success-modal .modal-body').html(data.error);
                }

                $('#confirmation-modal').modal('hide');
                $('#confirmation-success-modal').modal('show');
            });
        }
    });

})

function deactivate_user(self) {
    $('#ajaxModal').remove();

    var remote = self.data('remote');
    var modal = $('<div class="modal" id="ajaxModal"><div class="modal-body"></div></div>');
    modal.modal({backdrop: 'static', keyboard: false});
    modal.load(remote);
}

function action_modal(id, url, action) {
    $.post(url, {id: id, action: action, status: 0}, function (data) {
        $('#confirmation-action-title').html(data.action.title);
        $('#confirmation-title').html(data.obj.title);
        $('#confirmation-id').val(data.obj.id);
        $('#confirmation-action').val(data.action.action);
        $('#confirmation-url').val(data.action.url);

        if (data.warning) {
            $('#confirmation-warning-content').html(data.warning);
            $('#confirmation-warning').show();
        } else {
            $('#confirmation-warning').hide();
        }

        $('#confirmation-modal').modal('show');
    });
}