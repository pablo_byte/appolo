
var oLog;

$(function(){
    
    oLog = $('#log-table').DataTable({
        dom : 'rtp',
        serverSide: true,
        processing: true,
        ajax: {
            url: 'log/log_table/' + $('#startDate').val() + '/' + $('#endDate').val() + '/' + $('#level').val() + '/' + $('#controller').val(),
            type: 'POST'
        },
        order: [0,'desc']
    });
    
    function reloadLog() {
        var url = 'log/log_table/' + $('#startDate').val() + '/' + $('#endDate').val() + '/' + $('#level').val() + '/' + $('#controller').val();
        oLog.ajax.url(url).load();
    }
    
    $('#level').change(function(){
        reloadLog();
    });
    
    $('#controller').change(function(){
        reloadLog();
    });
    
    $('input[name="daterange"]').daterangepicker({
        format: 'YYYY/MM/DD HH:mm',
        timePicker: true,
        ranges: {
            'Hoy': [moment().startOf('day'),moment().endOf('day')],
            'Ayer': [moment().subtract('days',1).startOf('day'),moment().subtract('days',1).endOf('day')],
            '7 Días': [moment().subtract('days',6),moment()],
            '30 Días': [moment().subtract('days',29),moment()],
            'Este Mes': [moment().startOf('month'),moment().endOf('month')],
            'Último Mes': [moment().subtract('month',1).startOf('month'),moment().subtract('month',1).endOf('month')]
        }
    }, function(start, end){
        var dateStart = new Date(start);
        var dateEnd = new Date(end);
        
        $('#startDate').val(dateStart.getTime() / 1000);
        $('#endDate').val(dateEnd.getTime() / 1000);
        
        reloadLog();
    });
    
});
