var oType;

$(function(){
    
    oType = $('#farmacos-table').DataTable({
        dom : 'rtp',
        serverSide : true,
        proccesing : true,
        ajax : {
            url : 'farmacos/farmaco_table',
            type : 'POST'
        },
        columnDefs : [
            {"bVisible": false, "aTargets": [ 0 ]},
            {orderable: false, targets : [3]}
        ]
        
    });
    
    
    $('#addNewType').click(function(){
        
        $('#addEditModal').html('Crear Tipo de Dispositivo');
        $('#saveType').val('add');
        $('#alertDiv').hide();
        $('#add-edit-modal').modal('toggle');
    });
    
    $('#addEditSubmit').click(function(){
        
        var data = {
            type_name : $('#type_name').val(),
            type_geo : $('#type_geo').val(),
            type_img : $('#type_img').val(),
            type_vid : $('#type_vid').val(),
            save_type : $('#saveType').val(),
            type_id : $('#type_id').val()
        };
        
        $.ajax({
            url : 'farmacos/save',
            type : 'POST',
            data : data,
            success : function(r)
            {
                if(r.status == 'success')
                {
                    oType.ajax.reload();
                    $('#add-edit-modal').modal('toggle');
                }
                
                if(r.message != '')
                {
                    $('#alertMessage').html(r.message);
                    $('#alertDiv').show();
                }
            }
        });
        
    });
});

function edit_farmaco(id) {
    $('#addEditModal').html('Editar Tipo de Dispositivo');
    $('#saveType').val('edit');
    $('#alertDiv').hide();
    
    $.post('farmacos/get_farmaco/'+id, function(r){
        console.log(r);
        
        $('#codigo_farmaco').val(r.codigo_farmaco);
        $('#nombre_farmaco').val(r.nombre_farmaco);
        $('#tipo_farmaco').val(r.tipo_farmaco);
        $('#id').val(r.id);
        
        $('#add-edit-modal').modal('show');
    });
}