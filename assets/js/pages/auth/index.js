var oUser;

$(function(){
    
    oUser = $('#user-table').DataTable({
        dom: 'rtp',
        pagingType: 'full_numbers',
        serverSide: true,
        processing: true,
        ajax: {
            url: 'auth/user_table/',
            type: 'POST'
        }
    });
    
});