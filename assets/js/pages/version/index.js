var oVersion;

$(function(){
    
    oVersion = $('#version-table').DataTable({
        dom: 'rtp',
        serverSide: true,
        processing: true,
        ajax: {
            url: 'version/table',
            type: 'POST'
        },
        columnDefs : [
            {orderable: false, targets: 5}
        ]
    });
    
    // Activate/Deactivate/Delete Version
    $('#confirmation-button').click(function(){
        var version_id = $('#confirmation-id').val();
        var action = $('#confirmation-action').val();
        
        if(version_id != '' && action != '')
        {
            $.post('version/' + action, {version_id: version_id, action: 1}, function(data){
                
                $('#confirmation-id').val('');
                $('#confirmation-action').val('');
                
                if(data.type == 'success')
                {
                    oVersion.ajax.reload();
                    $('#confirmation-success-modal .modal-body').html(data.message);
                    
                } else {
                    $('#confirmation-success-modal .modal-body').html(data.error);
                }
                
                $('#confirmation-modal').modal('hide');
                $('#confirmation-success-modal').modal('show');
            });
        }
    });
    
});

function action_version(version_id, action)
{
    $.post('version/' + action, {version_id: version_id, action: 0}, function(data){
        $('#confirmation-action-title').html(data.action.title);
        $('#confirmation-title').html(data.version.version);
        $('#confirmation-id').val(data.version.id);
        $('#confirmation-action').val(data.action.action);
        
        $('#confirmation-modal').modal('show');
    });
}