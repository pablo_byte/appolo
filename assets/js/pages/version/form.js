$(function(){
    
    $('#platform_os').change(function(){
        var os = $(this).val();
        if(os != '')
        {
            $.post(base_url + 'version/get_platform_options/' + os,function(data){
                $('#platform_ver').html(data.options);
                $('#platform_ver').removeAttr('disabled');
            })
        } else {
            $('#platform_ver').html('<option value="">Seleccionar OS</option>');
            $('#platform_ver').attr('disabled','disabled');
        }
            
    });
    
});