<?php

function make_navigation($make = TRUE) {
    if($make) {
        $CI =& get_instance(); 
        $CI->load->view('templates/nav');
        echo '<div class="col-lg-10">';
    } else {
        echo '<div class="col-lg-12">';
    }
}

function make_nav_item($url = '',$text = '',$extra = '') {
    $CI =& get_instance();
    $uri = $CI->uri->segment(1);
    $active = '';
    
    if($url != '') {
        $extract = explode('/',$url);
        if($extract[0] == $uri) {
            $active = 'active';
        }
    } else {
        if(!$uri) {
            $active = 'active';
        }
    }
    
    return '<li class="'.$active.'">'.anchor($url,$text,$extra).'</li>';
}

function make_nav_item_sub($url = '',$text = '', $subitems = array())
{
    $CI =& get_instance();
    $uri = $CI->uri->segment(1);
    $uri_string = $CI->uri->uri_string;
    
    $active = '';
    
    if($url != '') {
        $extract = explode('/',$url);
        if(strpos($uri_string,$url) !== FALSE) {
            $active = 'active';
        }
    } else {
        if(!$uri) {
            $active = 'active';
        }
    }
    
    $items = '';
    
    foreach($subitems as $sub)
    {
        $items .= make_nav_item($sub[0],$sub[1]);
    }
    
    return '<li class="'.$active.'">'.anchor($url,$text).'<ul>'.$items.'</ul></li>';
}