<?php defined('BASEPATH') OR exit('No direct script access allowed');

abstract class AR_class extends CI_Model{
    
    protected $table = NULL;
       
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    /*
     * Select data from $table based on 
     * $conditions = array(
     *              'where' => array(`column` => `value`,...) ONLY USE FOR AND!,
     *              'order' => array(`column` => ASC, DESC or RANDOM),
     *              'limit' => 'from, number_of_rows'
     */
    function select_simple($conditions = NULL, $prot = TRUE){

        $this->db->select(isset($conditions['select']) ? $conditions['select'] : "*");
        
        if(isset($conditions['where']) && !empty($conditions['where']))
            $this->db->where($conditions['where'],NULL,$prot);
        
        if(isset($conditions['wherein']) && !empty($conditions['wherein']))  {
            $this->db->where_in($conditions['wherein']);
        }
        if(isset($conditions['like']) && !empty($conditions['like']))  {
            $this->db->like($conditions['like']);
        } 
        
        /*vijeesh code */
        if(!empty ($conditions['join'])) 
        {
            foreach($conditions['join'] as $key=>$value){
                 $tabname = $joinfield = $jointype= "";
                 foreach($value as $inkey=>$invalue){
                       
                     if ( ($inkey == 'tbname') && $invalue != "" ) {
                           $tabname = $invalue;
                     }
                     if ( ($inkey == 'joinfield') && $invalue != "" ) {
                           $joinfield = $invalue;
                     }
                     if ( ($inkey == 'jointype') && $invalue != "" ) {
                          $jointype = $invalue;
                     }
                      
                  }
                  $this->db->join($tabname, $joinfield, $jointype);
                }
        }  
        /* end vijeesh code */
        
        if(!empty($conditions['order'])) 
        {
            foreach($conditions['order'] as $order => $by){
                $this->db->order_by($order, $by);
            }
        }
        
        if(!empty($conditions['group_by'])){
             $this->db->group_by($conditions['group_by']);
        }
        
        if(!empty($conditions['limit']))
        {
            $limit = explode(",", $conditions['limit']);
            if(count($limit) > 1){
                if(!empty($conditions['table']))
                {
                    $query = $this->db->get($conditions['table'],$limit[1],$limit[0]);
                } 
                else
                $query = $this->db->get($this->table, $limit[1], $limit[0]);
            }else{
                
                $query = $this->db->get($this->table,$limit[0],0);
            }
        }
        else
        {
            if(!empty($conditions['table']))
                $query = $this->db->get($conditions['table']);
            else
                $query = $this->db->get($this->table);
        }
        
        if($query) {
            $result =  $query->result();
            //echo  $this->db->last_query();  
            return $result;
        }    
        return array();
    }
    
    /*
     * Select data by query.
     * For complex queries
     */
    function select_by_query($query){
        $query = $this->db->query($query);
        if($query)
            return $query->result();
        return array(0); 
    }
    
    function run_query($query){
        $query = $this->db->query($query);
        if($query)
            return TRUE;
        return FALSE; 
    }
    
    /*
     * Insert $data into $table.
     * $data = array(`column` => `value`, ...)
     * If we want to insert multiple values use "insert_more"
     */        
    function insert_it($data){
        $this->db->query("SET foreign_key_checks = 0");
        $query =  $this->db->insert($this->table, $data);
        //added by vijeesh
        $insert_id = $this->db->insert_id();
        //vijeesh code end
        $this->db->query("SET foreign_key_checks = 1");
        if($query)
                return $insert_id;
        return FALSE;
    }
    
    
    /*
     * Insert multiple data into table
     */
    
    function insert_all($data){
        
        $query =  $this->db->insert_batch($this->table, $data);
        if($query)
            return TRUE;
        return FALSE;
    }
    
    /*
     * Update $data into $table with $were condition
     * $data = array(`column` => `value`, ...)
     * $where = array(`column` => `value`,...)
     */ 
    function update($data, $where=NULL){
        if($this->db->update($this->table, $data, $where))
                return TRUE; 
        return FALSE;
    }
    
    /*
     * Delete from $table with $were condition
     * $where = array(`column` => `value`,...)
     * if $where is not set then empty table;
     */ 
    function delete($where){
        if(isset($where))
            $this->db->where($where);
        if($this->db->delete($this->table))
                return TRUE; 
        return FALSE;
    }
    
    /*
     * Delete from $table with $were condition
     * $where = array(`column` => `value`,...)
     * if $where is not set then empty table;
     */ 
    function truncate($table){
        if($this->db->truncate($table))
                return TRUE; 
        return FALSE;
    }
    
    function dropdown($conditions = NULL)
    {
        $temp = $this->select_simple($conditions);
        $vars = explode(",", $conditions['select']);
        if($temp){
            foreach($temp as $t_object){
                $t = (array) $t_object;
                $tmp1 = explode(".", $vars[0]);
                $tmp1 = count($tmp1)>1 ? $tmp1[1] : $tmp1[0];
                $tmp2 = explode(".", $vars[1]);
                $tmp2 = count($tmp2)>1 ? $tmp2[1] : $tmp2[0];
                
                $dropdown[$t[trim($tmp1)]] = $t[trim($tmp2)];
            }
            return $dropdown;
        }
        return array();
    }
        
    function dropdown_selected($conditions = NULL){
        $dropdown_selected = $this->select_simple($conditions);
        $vars = explode(",", $conditions['select']);
        if(isset($dropdown_selected[0]))
            return $dropdown_selected[0]->$vars[0];
        return 0; 
    }
    
    function graph_stats($conditions = array())
    {
        
        if(!is_array($conditions) || count($conditions)==0){
            return FALSE;
        }
        
        $interval = "(select 0 N union all\n";
        for($i = 1; $i<$conditions['count_interval']; $i++){
            $interval .= "select -$i union all\n";
        }
        $interval .= "select -{$conditions['count_interval']}) N) N\n";
        
        switch($conditions['frame']){
            case "month":
                $frame = "%Y-%m";
                $frame_interval = "MONTH";
                break;
            case "day":
                $frame = "%Y-%m-%d";
                $frame_interval = "DAY";
                break;
            case "hour":
                $frame = "%Y-%m-%d %H:00:00";
                $frame_interval = "HOUR";
                break;
        }
        $order_by = "";
        if($conditions['filter_by'] == 1 || $conditions['filter_by'] == 2) {
            $order_by = " ORDER BY alert_time DESC";
            $time_format = $conditions['is_timestamp'] ? "FROM_UNIXTIME($this->table.{$conditions['time_column']}" : "$this->table.{$conditions['time_column']}";

            $query = "SELECT DATE_FORMAT(N.PivotDate,'$frame') AS {$conditions['frame_alias']}, COUNT($this->table.{$conditions['count']}) AS {$conditions['count_alias']}
            FROM (
            select ADDDATE(NOW(), INTERVAL N $frame_interval) PivotDate
            FROM $interval
            LEFT JOIN $this->table ON {$conditions['where']} {$conditions['wherein']}
                AND DATE_FORMAT(N.PivotDate,'$frame')=DATE_FORMAT($time_format,'$frame')
            GROUP BY {$conditions['frame_alias']} $order_by ";
        }
        else if($conditions['filter_by'] == 3) {
            $order_by = " ORDER BY timediff DESC, alerta.riesgo ASC";
            $query = "SELECT alerta.id,alerta.riesgo as risk_id,area_riesgos.riesgo as risk_name, AVG(TIME_TO_SEC(TIMEDIFF(alerta.updated_at,alerta.created_at))) as timediff 
            FROM alerta
            LEFT JOIN area_riesgos on area_riesgos.id=alerta.riesgo AND area_riesgos.area_id = alerta.area_id
            WHERE {$conditions['where']} AND alerta.created_at >= (NOW() - INTERVAL {$conditions['count_interval']} DAY) {$conditions['wherein']} GROUP BY {$conditions['frame_alias']} 
            $order_by";
           // echo $query; 
        }
        //echo $query;
        //echo "<br/>";
        $query = $this->db->query($query);
        
        if($query)
            return $query->result();
        return array(); 
    }
}