<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'libraries/AR_Class.php');

class Farmaco extends AR_class {

    function __construct() {
        parent::__construct();
        $this->table = 'farmacos';
    }

    function get($order = 'nombre_farmaco', $orberby = 'DESC', $limit = '0,10', $where = array()) {

        $conditions = array(
            'select' => '*',
            'table' => $this->table,
            'order' => array($order => $orberby),
            'limit' => $limit
        );

        $_levels = array(1 => 'ERROR', 2 => 'DEBUG', 3 => 'INFO', 4 => 'ALL');


        if (count($where) > 0) {
            $conditions['where'] = '';
            $and = '';

            if (isset($where['start']) && $where['start'] > 0) {
                $start = date('Y-m-d H:i:s', $where['start']);
                $end = date('Y-m-d H:i:s', $where['end']);
                $conditions['where'] .= "created BETWEEN '{$start}' AND '{$end}' ";
                $and = 'AND';
            }

            if (isset($where['level']) && $where['level'] != 'all' && $where['level'] != 'undefined') {
                $level = $_levels[$where['level']];
                $conditions['where'] .= $and . " level = '$level' ";
                $and = 'AND';
            }

            if (isset($where['controller']) && $where['controller'] != 'all' && $where['controller'] != 'undefined') {
                $conditions['where'] .= $and . " controller = '{$where['controller']}'";
            }
        }

        return $this->select_simple($conditions, FALSE);
    }
    
    function get_farmaco($id_farmaco = FALSE, $order = FALSE, $orderby = 'ASC', $limit = '1') {
        $this->table = 'farmacos';
        
        $conditions = array();
        
        if($id_farmaco) {
            $conditions['where'] = array('id_farmaco' => $id_farmaco);
        }
        
        if($order){
            $conditions['order'] = array(
                $order  => $orderby
            ); 
        }
        
        $conditions['limit'] = $limit;
        
        return $this->select_simple($conditions);
    }

    function count($where = array()) {

        $conditions = array(
            'select' => 'COUNT(*) as count'
        );

        $_levels = array(1 => 'ERROR', 2 => 'DEBUG', 3 => 'INFO', 4 => 'ALL');


        if (count($where) > 0) {
            $conditions['where'] = '';
            $and = '';

            if (isset($where['start']) && $where['start'] > 0) {
                $start = date('Y-m-d H:i:s', $where['start']);
                $end = date('Y-m-d H:i:s', $where['end']);
                $conditions['where'] .= "created BETWEEN '{$start}' AND '{$end}' ";
                $and = 'AND';
            }

            if (isset($where['level']) && $where['level'] != 'all' && $where['level'] != 'undefined') {
                $level = $_levels[$where['level']];
                $conditions['where'] .= $and . " level = '$level' ";
                $and = 'AND';
            }

            if (isset($where['controller']) && $where['controller'] != 'all' && $where['controller'] != 'undefined') {
                $conditions['where'] .= $and . " controller = '{$where['controller']}'";
            }
        }

        $count = $this->select_simple($conditions, FALSE);

        if (count($count) > 0) {
            return $count[0]->count;
        }

        return 0;
    }
    
    function insert_farmaco($data) {
        $this->table = 'farmacos';
        return $this->insert_it($data);
    }
    
    function update_farmaco($data, $where) {
        $this->table = 'farmacos';
        return $this->update($data,$where);
    }
    
    function delete_farmaco($id_farmaco) {
        $this->table = 'farmacos';
        return $this->delete(array('id_farmaco' => $id_farmaco));
    }

    function get_controllers($drop = FALSE) {

        $sql = "SELECT DISTINCT controller FROM $this->table";
        $query = $this->select_by_query($sql);

        if ($drop) {
            $return = array();
            foreach ($query as $con) {
                $return[$con->controller] = $con->controller;
            }
            return $return;
        }

        return $query;
    }

}
