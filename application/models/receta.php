<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'libraries/AR_Class.php');

class Receta extends AR_class {

    function __construct() {
        parent::__construct();
        $this->table = 'recetas';
        $this->item = 'receta_items';
        $this->farmacos = 'farmacos';
    }
    
    function get_receta($id_receta = NULL, $order = NULL) {
        
        if(isset($id_receta)) array();
        
        $sql = "SELECT r.id_receta, r.tipo_receta, r.rut_doctor, r.rut_paciente, r.nombre_paciente, r.edad_paciente, r.direccion_paciente, r.detalle_receta, r.created "
            . "FROM $this->table r "
            . "WHERE r.id_receta = {$id_receta}";
        
        if(isset($order)) {
            $sql .= "ORDER BY {$order} ";
        }
        
        //echo $sql;
        $res = $this->select_by_query($sql);
        
        if(count($res) == 0) return array();
                
        return $res[0];
    }
    
    function get($order = 'fecha_receta', $orberby = 'DESC', $limit = '0,10', $where = array()) {

        $conditions = array(
            'select' => '*',
            'table' => $this->table,
            'order' => array($order => $orberby),
            'limit' => $limit
        );

        if (count($where) > 0) {
            $conditions['where'] = '';
            $and = '';

            if (isset($where['start']) && $where['start'] > 0) {
                $start = date('Y-m-d H:i:s', $where['start']);
                $end = date('Y-m-d H:i:s', $where['end']);
                $conditions['where'] .= "created BETWEEN '{$start}' AND '{$end}' ";
                $and = 'AND';
            }

            if (isset($where['rut_doctor']) && $where['rut_doctor'] != '') {
                $conditions['where'] .= $and . " rut_doctor = '{$where['rut_doctor']}' ";
                $and = 'AND';
            }

            /*if (isset($where['controller']) && $where['controller'] != 'all' && $where['controller'] != 'undefined') {
                $conditions['where'] .= $and . " controller = '{$where['controller']}'";
            }*/
        }
        
        return $this->select_simple($conditions, FALSE);
    }
    
    function get_items($id_receta = NULL, $order = NULL) {
        
        if(isset($id_receta)) array();
        
        $sql = "SELECT i.id, i.id_receta, i.id_farmaco, f.nombre_farmaco, f.tipo_farmaco, i.dosis, i.cant_total, i.estado_item "
            . "FROM $this->item i "
            . "LEFT JOIN $this->farmacos f ON f.id_farmaco = i.id_farmaco "
            . "WHERE i.id_receta = {$id_receta}";
        
        if(isset($order)) {
            $sql .= "ORDER BY {$order} ";
        }
        
        //echo $sql;
        $res = $this->select_by_query($sql);
        
        if(count($res) == 0) return array();
                
        return $res;
    }
    
    /*function get_items($order = 'nombre_farmaco', $orberby = 'DESC', $limit = '0,10', $where = array()) {

        $conditions = array(
            'select' => '*',
            'table' => $this->item,
            'order' => array($order => $orberby),
            'join'  => array(
                array(
                    'tbname' => 'farmacos',
                    'joinfield' => 'receta_items.id_farmaco =  farmacos.id_farmaco',
                    'jointype' => 'left'
                ),
            ),
            'limit' => $limit
        );
        
        $_levels = array(1 => 'ERROR', 2 => 'DEBUG', 3 => 'INFO', 4 => 'ALL');

        if (count($where) > 0) {
            $conditions['where'] = '';
            $and = '';

            if (isset($where['start']) && $where['start'] > 0) {
                $start = date('Y-m-d H:i:s', $where['start']);
                $end = date('Y-m-d H:i:s', $where['end']);
                $conditions['where'] .= "created BETWEEN '{$start}' AND '{$end}' ";
                $and = 'AND';
            }
            
            if (isset($where['receta_items.id_receta']) && $where['receta_items.id_receta'] != '') {
                $id_receta = $where['receta_items.id_receta'];
                $conditions['where'] .= $and . " id_receta = '$id_receta' ";
            }
        }
        
        return $this->select_simple($conditions, FALSE);
    }*/
    
    function get_recetas($id_receta = FALSE, $order = FALSE, $orderby = 'ASC', $limit = '1')
    {
        $this->table = 'recetas';
        
        $conditions = array();
        
        if($id) {
            $conditions['where'] = array('id_receta' => $id_receta);
        }
        
        if($order){
            $conditions['order'] = array(
                $order  => $orderby
            ); 
        }
        
        $conditions['limit'] = $limit;
        
        return $this->select_simple($conditions);
    }

    function count($where = array()) {

        $conditions = array(
            'select' => 'COUNT(*) as count'
        );

        $_levels = array(1 => 'ERROR', 2 => 'DEBUG', 3 => 'INFO', 4 => 'ALL');


        if (count($where) > 0) {
            $conditions['where'] = '';
            $and = '';

            if (isset($where['start']) && $where['start'] > 0) {
                $start = date('Y-m-d H:i:s', $where['start']);
                $end = date('Y-m-d H:i:s', $where['end']);
                $conditions['where'] .= "created BETWEEN '{$start}' AND '{$end}' ";
                $and = 'AND';
            }

            if (isset($where['level']) && $where['level'] != 'all' && $where['level'] != 'undefined') {
                $level = $_levels[$where['level']];
                $conditions['where'] .= $and . " level = '$level' ";
                $and = 'AND';
            }

            if (isset($where['controller']) && $where['controller'] != 'all' && $where['controller'] != 'undefined') {
                $conditions['where'] .= $and . " controller = '{$where['controller']}'";
            }
        }

        $count = $this->select_simple($conditions, FALSE);

        if (count($count) > 0) {
            return $count[0]->count;
        }

        return 0;
    }
    
    function insert_farmaco($data) {
        $this->table = 'recetas';
        return $this->insert_it($data);
    }
    
    function update_farmaco($data, $where) {
        $this->table = 'recetas';
        return $this->update($data,$where);
    }
    
    function delete_farmaco($id) {
        $this->table = 'recetas';
        return $this->delete(array('id' => $id));
    }
    
}
