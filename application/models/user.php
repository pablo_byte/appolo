<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'libraries/AR_Class.php');

class User extends AR_class {
    function __construct() {
        parent::__construct();
        $this->table = 'users';
    }
    
    public function activate_user($user_id = null, $data = array()) {

        if($user_id == null){
            return false;
        }
        
        return $this->db->update($this->table, $data, array('user_id' => $user_id));
        
    }
    
    function get($user_id = NULL, $token = NULL, $phone = NULL) {
        
        
        $conditions = array(
            'select'    => '*',
        );
        
        if($user_id != "") {
            $conditions = array(
                "where" => array('user_id' => $user_id),
            );
        }
        
        if($token != "") {
            $where_token = '"token":"'.$token.'"';
            $conditions = array(
                "like" => array('auth_token' => $this->db->escape_like_str($where_token)),
            );
        }
        
        if($phone != "") {
            $conditions = array(
                "where" => array('username' => $phone),
            );
        }
        
        $res = $this->select_simple($conditions);
        //echo $this->db->last_query();die();
        return $res;
    }
    
    public function get_users($select = null, $where = null, $search = null, $sort = null, $limit = null, $ext_join = null, $ext_search_fields = array() ) {
        //setting select fields to query
        if($select == "") {
            $sql = "SELECT users.id, users.user_id, users.username, users.email, users.first_name, users.last_name, users.active, users.phone, /*users.avatar,*/ users.created_on";
        } else {//setting user defined select fields to query
            $sql = "SELECT $select ";
        }
        //adding main table and joins to query
        $sql .= " FROM $this->table ";


        //adding extra joins to query
        if($ext_join != "") {
            $sql .= " $ext_join ";
        }
        //adding where condition to query
        if($where != "") {
            $sql .= " WHERE $where";
        }
        $ext_search = "";
        if(!empty($ext_search_fields)) {
            $and = "";

            if(isset($ext_search_fields['user_id']) && $ext_search_fields['user_id'] != "") {
                if($ext_search != "") {
                    $and = " AND ";
                }
                $ext_search .= $and." users.user_id LIKE '{$ext_search_fields['user_id']}' ";  
            }

        }
        //search filter condition end

        if($ext_search != "") {
            if($where == "" )
                $sql .= " WHERE ".$ext_search;
            else
                $sql .= " AND ".$ext_search;
        } 
        //adding search condition to query
        if($search != "") {
            if($where == "")
                $sql .= " WHERE (users.user_id LIKE '$search') ";
            else
                $sql .= " AND (users.user_id LIKE '$search') ";
        }
        //adding sort fields to query
        if($sort != "") {
            $sql .= " ORDER BY $sort ";
        }    
        if($limit != "") {
            $sql .= " LIMIT $limit ";
        }
        //setting and executng entire query and return result
        
        
        //echo $sql;//die();
        $query = $this->db->query($sql);
        if($query) {
            return $query->result();
        }
        else
            return array();
    }
    
    function get_verification_code($user_id) {
        $sql = "SELECT users.id, users.activation_code FROM users WHERE users.user_id = '".$user_id."' LIMIT 1";
        
        //echo $sql;die();
        $query = $this->db->query($sql);
        if($query) {
            
            $res = $query->result();
            preg_match_all('!\d+!', $res[0]->activation_code, $aux);//print_r($aux);
            $activation_code = substr(implode('', $aux[0]), 0, 6);//print_r($activation_code);
            
            
            return $activation_code;
            
        }  else {
            
            return array();
        }
    }
    
    function count() {
        
        $conditions = array(
            'select'    => 'COUNT(*) as count'
        );
        
        $query = $this->select_simple($conditions);
        
        if(count($query)>0) {
            return $query[0]->count;
        }
        
    }
    
    function generate_id()
    {
        //TO DO - create reverse base_url
        try
        {
            $host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
            if($host == $_SERVER['REMOTE_ADDR'])
            {
                $country = "cl";
            }
            else
            {
                //$country = @geoip_country_code_by_name($host);
                $country = "cl";
            }
        }
        catch (Exception $ex)
        {
            $country = "cl";
        }
        
        $user_id = uniqid() . ".$country.skynouk.user";
        
        $unique = $this->select_simple(array('select' => 'id', "where" => array('user_id' => $user_id)));
        
        while(count($unique) > 0) $user_id = uniqid() . ".$country.skynouk";
        
        return $user_id;
    }
    
    function generate_encryption_keys($user_id)
    {
        
    }
    
    
}
