<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Log</h1>
        <ol class="breadcrumb">
            <li class="active">Log</li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="row">
            <div class="form-group col-lg-3">
                <label>Fecha/Hora</label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                <?php echo form_input($date);?>
                </div>
                <input type="hidden" value="0" id="startDate">
                <input type="hidden" value="0" id="endDate">
            </div>
            <div class="form-group col-lg-3">
                <label>Nivel</label>
                <?php echo form_dropdown('level',$level_drop,'all','class="form-control" id="level"');?>
            </div>
            <div class="form-group col-lg-3">
                <label>Controlador</label>
                <?php echo form_dropdown('controller',$controller_drop,'all','class="form-control" id="controller"'); ?>
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped" id="log-table">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Nombre</th>
                        <th>Tipo</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/js/pages/farmacos/index.js');?>" type="text/javascript"></script>