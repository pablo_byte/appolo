<!DOCTYPE html>
<html lang="es">
    <head>
        
        <base url="<?php echo base_url();?>">
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA_Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        
        <title><?php echo $title;?></title>
        
        <!-- CSS -->
        <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
        <!--<link href="<?php echo base_url();?>assets/css/sb-admin.css" rel="stylesheet">-->
        <link href="<?php echo base_url();?>assets/css/theme.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!--<link href="<?php echo base_url();?>assets/css/plugins/datatables/css/jquery.dataTables.min.css" rel="stylesheet">-->
        <link href="<?php echo base_url();?>assets/css/plugins/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
        <link href="<?php echo base_url();?>assets/css/plugins/daterangepicker-bs3.css" rel="stylesheet">
        
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <!-- JS -->
        <script>var base_url = "<?php echo base_url();?>";</script>
        <script src="<?php echo base_url();?>assets/js/jquery.js"></script>
        <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
        
        <script src="<?php echo base_url();?>assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/plugins/datatables/dataTables.bootstrap.min.js"></script>
        
        <script src="<?php echo base_url();?>assets/js/plugins/moment.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/plugins/daterangerpicker.js"></script>
    </head>

    <body>
        <div id="wrapper"> <!-- closing in footer -->
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="<?php echo base_url();?>"><i class="fa fa-lock fa-fw"></i> Appolo</a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#">About</a></li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" aria-expanded="false" role="button" data-toggle="dropdown" href="#">Recetas <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <a href="<?php echo base_url("recetas/"); ?>">Recetas </a>
                                    <li><a href="<?php echo base_url("recetas/new_receta"); ?>">Nueva Receta</a></li>
                                    <li class="divider"></li>
                                    <li><a href="<?php echo base_url(""); ?>">Otros</a></li>
                                </ul>
                                
                            </li>
                            <li class="dropdown">
                                <a class="dropdown-toggle" aria-expanded="false" role="button" data-toggle="dropdown" href="#">Administración <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="<?php echo base_url("auth/"); ?>">Usuarios</a></li>
                                    <li><a href="<?php echo base_url("farmacos/"); ?>">Fármacos</a></li>
                                    <li class="divider"></li>
                                    <li class="dropdown-header">Sub Admin</li>
                                    <li><a href="<?php echo base_url(""); ?>">Otros</a></li>
                                </ul>
                                
                            </li>
                            
                        </ul>
                        <!-- Top Menu Items -->
                        <ul class="nav navbar-right top-nav">
                            <!--
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                                <ul class="dropdown-menu alert-dropdown">
                                    <li><a href="#">Alert Name <span class="label label-default">Alert Badge</span></a></li>
                                    <li><a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a></li>
                                    <li><a href="#">Alert Name <span class="label label-success">Alert Badge</span></a></li>
                                    <li><a href="#">Alert Name <span class="label label-info">Alert Badge</span></a></li>
                                    <li><a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a></li>
                                    <li><a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a></li>
                                    <li class="divider"></li>
                                    <li><a href="#">View All</a></li>
                                </ul>
                            </li>
                            -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $user->first_name.' '.$user->last_name;?> <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <!--li><a href="#"><i class="fa fa-fw fa-user"></i> Profile</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a></li>
                                    <li><a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a></li>
                                    <li class="divider"></li-->
                                    <li><?php echo anchor('auth/logout','<i class="fa fa-fw fa-power-off"></i> Cerrar Sesión');?></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div id="page-wrapper">
               <div class="container">
                   <?php make_navigation($nav);?>
           
