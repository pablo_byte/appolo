<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Fármacos</h1>
        <ol class="breadcrumb">
            <li class="active">Administracion</li>
            <li class="active">Fármacos</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
    <?php if(isset($message) && !empty($message)) {?>
        <div class="alert alert-danger"><?php echo $message;?></div>
    <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <button class="btn btn-info pull-right" id="addNewType"><i class="fa fa-plus"></i> Nuevo Fármaco</button>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-striped" id="farmacos-table">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Código</th>
                        <th>Nombre</th>
                        <th>Tipo</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal fade" id="confirmation-modal" tabindex="-1" role="dialog" aria-labelledby="confirmationModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="confirmationModal">Confirmar</h4>
            </div>
            <div class="modal-body">
                ¿Estás seguro de querer <span id="confirmation-action-title"></span> "<span id="confirmation-title" style="font-weight: bold"></span>"?
                <p></p>
                <div class="alert alert-danger" id="confirmation-warning" style="display: none">
                    <p id="confirmation-warning-content"></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-info" id="confirmation-button">Confirmar</button>
                <input type="hidden" id="confirmation-id" name="id" value="" />
                <input type="hidden" id="confirmation-action" name="action" value="" />
                <input type="hidden" id="confirmation-url" name="url" value="" />
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="confirmation-success-modal" tabindex="-1" role="dialog" aria-labelledby="confirmationSuccessModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add-edit-modal" tabindex="-1" role="dialog" aria-labelledby="addEditModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="addEditModal">Confirmar</h4>
            </div>
            <div class="modal-body">
                <div class="row" id="alertDiv" style="display:none">
                    <div class="col-lg-12">
                        <div class="alert alert-danger" id="alertMessage"></div>
                    </div>
                </div>
                <?php echo form_open(current_url(),'class="form-horizontal"');?>
                <div class="form-group">
                    <label class="control-label col-md-5">Código Fármaco</label>
                    <div class="col-md-7">
                        <input type="text" name="codigo_farmaco" value="" class="form-control" placeholder="Ej: 001" id="codigo_farmaco" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-5">Nombre Fármaco</label>
                    <div class="col-md-7">
                        <input type="text" name="nombre_farmaco" value="" class="form-control" placeholder="Ej: Sensor de Movimiento" id="nombre_farmaco" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-5">Tipo Administración</label>
                    <div class="col-md-6">
                        <select name="tipo_administracion" class="form-control" id="tipo_administracion">
                            <option value="0">No tiene</option>
                            <option value="1">Oral</option>
                            <option value="2">Sublingual</option>
                            <option value="3">Parenteral</option>
                        </select>
                    </div>
                </div>
                <input type="hidden" name="save_type" id="saveType" value="" />
                <input type="hidden" name="type_id" id="type_id" value="" />
                <?php echo form_close();?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="addEditSubmit" data-type="">Guardar</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url('assets/js/pages/common.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/pages/farmaco/index.js');?>" type="text/javascript"></script>