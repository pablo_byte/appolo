<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"><?php echo lang('index_heading');?></h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-dashboard"></i> <?php echo lang('index_heading');?></li>
        </ol>
    </div>
</div> <!-- /.row -->

<?php if(isset($message) && !empty($message)) {?>
<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-info alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $message;?>
        </div>
    </div>
</div>
<!-- /.row -->
<?php } ?>

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <table class="table table-hover table-striped" id="user-table">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Objetos</th>
                        <th>Dispositivos</th>
                        <th>Eventos</th>
                        <th>Estado</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
<script src="<?php echo base_url('assets/js/pages/common.js');?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/pages/auth/index.js');?>" type="text/javascript"></script>