<div class="container">    
    <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-8 col-md-offset-2">
        <div class="panel panel-info" >
            <div class="panel-heading">
                <div class="panel-title">Ingresar</div>
            </div>
            <div style="padding-top:30px" class="panel-body" >
                <div class="col-lg-8 col-lg-offset-2">
                    <h1 class="page-header"><?php echo lang('login_heading'); ?></h1>
                    <?php if (isset($message) && !empty($message)) { ?>
                        <div class="alert alert-danger"><?php echo $message; ?></div>
                    <?php } ?>
                    <p><?php echo lang('login_subheading'); ?></p>

                    <?php echo form_open("auth/login", 'role="form"'); ?>
                    <div class="form-group">
                        <label><?php echo lang('login_identity_label', 'identity'); ?></label>
                        <?php echo form_input($identity); ?>
                    </div>
                    <div class="form-group">
                        <label><?php echo lang('login_password_label', 'password'); ?></label>
                        <?php echo form_input($password); ?>
                    </div>

                    <div class="form-group">
                        <label><?php echo lang('login_remember_label', 'remember'); ?></label>
                        <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"'); ?>
                    </div>


                    <div class="form-group">
                        <?php echo form_submit('submit', lang('login_submit_btn'), 'class="btn btn-primary"'); ?>
                    </div>

                    <?php echo form_close(); ?>

                    <p><a href="forgot_password"><?php echo lang('login_forgot_password'); ?></a></p>
                </div>
            </div>
        </div>
    </div>
</div>