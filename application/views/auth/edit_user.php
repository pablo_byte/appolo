<!-- Page Heading-->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header"> <?php echo lang('edit_user_heading');?></h1>
        <ol class="breadcrumb">
            <li><?php echo lang('index_heading');?></li>
            <li class="active"><?php echo lang('edit_user_heading');?></li>
        </ol>
    </div>
</div>

<?php if(isset($message) && !empty($message)) {?>
<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo $message;?>
        </div>
    </div>
</div>
<?php } ?>

<div class="row">
    <div class="col-lg-4">
        <p><?php echo lang('edit_user_subheading');?></p>

        <?php echo form_open(uri_string(),'role="form"');?>

        <div class="form-group">
            <?php echo lang('edit_user_fname_label', 'first_name');?> <br />
            <?php echo form_input($first_name);?>
      </div>

      <div class="form-group">
            <?php echo lang('edit_user_lname_label', 'last_name');?> <br />
            <?php echo form_input($last_name);?>
      </div>

      <div class="form-group">
            <?php echo lang('edit_user_company_label', 'company');?> <br />
            <?php echo form_input($company);?>
      </div>

      <div class="form-group">
            <?php echo lang('edit_user_phone_label', 'phone');?> <br />
            <?php echo form_input($phone);?>
      </div>

      <div class="form-group">
            <?php echo lang('edit_user_password_label', 'password');?> <br />
            <?php echo form_input($password);?>
      </div>

      <div class="form-group">
            <?php echo lang('edit_user_password_confirm_label', 'password_confirm');?><br />
            <?php echo form_input($password_confirm);?>
      </div>

      <?php echo form_hidden('id', $user->id);?>
      <?php echo form_hidden($csrf); ?>

      <div class="form-group"><?php echo form_submit('submit', lang('edit_user_submit_btn'),'class="btn btn-primary"');?></div>

<?php echo form_close();?>
    </div>
</div>
