<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4><?php echo lang('deactivate_heading');?></h4>
        </div>
        <?php echo form_open("auth/deactivate/".$user->id);?>
        <div class="modal-body">
            <p><?php echo sprintf(lang('deactivate_subheading'), $user->username);?></p>

            

              <p>
                    <?php echo lang('deactivate_confirm_y_label', 'confirm');?>
                <input type="radio" name="confirm" value="yes" checked="checked" />
                <?php echo lang('deactivate_confirm_n_label', 'confirm');?>
                <input type="radio" name="confirm" value="no" />
              </p>

              <?php echo form_hidden($csrf); ?>
              <?php echo form_hidden(array('id'=>$user->id)); ?>

              <p></p>

            
        </div>
        <div class="modal-footer">
            <?php echo form_submit('submit', lang('deactivate_submit_btn'),'class="btn btn-primary"');?>
            <a href="#" class="btn btn-primary" data-dismiss="modal">Cancelar</a>
        </div>
        <?php echo form_close();?>
    </div>
</div>
