<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <ol class="breadcrumb">
            <li class="active">Recetas</li>
            <li class="active">Detall Receta</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?php if (isset($message) && !empty($message)) { ?>
            <div class="alert alert-danger"><?php echo $message; ?></div>
        <?php } ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Detalle Receta: <?php echo $id_receta; ?> <span class="pull-right text-primary">Tipo: Retenida</span></h3>
            </div>
            <div class="panel-body">

            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <label class="col-lg-3">Doctor</label>
                    <span class="col-lg-3"><?php echo $data_receta->tipo_receta; ?></span>
                    <label class="col-lg-2">Rut Doctor</label>
                    <span class="col-lg-2"><?php echo $data_receta->rut_doctor; ?></span>
                    <label class="col-lg-1">Fecha</label>
                    <span><?php echo date('d/m/Y', strtotime($data_receta->created)); ?></span>
                </li>
                <li class="list-group-item">
                    <label class="col-lg-3">Paciente</label>
                    <span class="col-lg-3"><?php echo $data_receta->nombre_paciente; ?></span>
                    <label class="col-lg-2">Rut Paciente</label>
                    <span class="col-lg-2"><?php echo $data_receta->rut_paciente; ?></span>
                    <label class="col-lg-1">Edad</label>
                    <span><?php echo $data_receta->edad_paciente; ?></span>
                </li>
                <li class="list-group-item">
                    <h4 class="col-lg-12">Detalle Receta</h4>
                    <div class="col-lg-12">
                        <textarea cols="120" rows="5" readonly="" id="detalle_receta"><?php echo html_escape($data_receta->detalle_receta); ?></textarea>
                        <?php echo display_ckeditor($ckeditor);?>
                    </div>
                </li>
            </ul>

            <table class="table table-striped" id="detalle-receta-table">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Código</th>
                        <th>Fármaco</th>
                        <th>Tipo</th>
                        <th>Dosis</th>
                        <th>Total</th>
                        <th>Estado</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>

</div>

<input type="hidden" id="id_receta" name="id_receta" value="<?php echo $id_receta; ?>"/>

<script src="<?php echo base_url('assets/js/pages/common.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/js/pages/receta/detalle_receta.js'); ?>" type="text/javascript"></script>
