<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!--h1 class="page-header"><?php echo $header_title;?></h1-->
        <ol class="breadcrumb">
            <li>Recetas</li>
            <li class="active"><?php echo $header_title;?></li>
        </ol>
    </div>
</div>

<div class="row">
    <div class="col-lg-8">
    <?php if(isset($message) && !empty($message)) {?>
        <div class="alert alert-danger"><?php echo $message;?></div>
    <?php } ?>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <?php echo form_open(current_url(), 'role="form" class="form-horizontal"');?>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Tipo Receta</label>
            <div class="col-sm-2">
                <?php echo form_dropdown('tipo_receta', $tipo_receta_options, '', 'class="form-control"');?>
            </div>
            <label class="col-sm-2 control-label">Fecha</label>
            <p class="col-sm-3 control-label"><?php echo $fecha; ?></p>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Rut Paciente</label>
            <div class="col-sm-2">
                <?php echo form_input($rut_paciente);?>
            </div>
            <label class="col-sm-2 control-label">Nombre Paciente</label>
            <div class="col-sm-6">
                <?php echo form_input($nombre_paciente);?>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-2 control-label">Edad Paciente</label>
            <div class="col-sm-2">
                <?php echo form_input($edad_paciente);?>
            </div>
            <label class="col-sm-2 control-label">Dirección</label>
            <div class="col-sm-6">
                <?php echo form_input($direccion_paciente);?>
            </div>
        </div>
        
        <div class="form-group">
            <label>Detalle Receta</label>
            <?php echo form_textarea($detalle_receta);?>
            <?php echo display_ckeditor($ckeditor);?>
        </div>
        
        <div class="form-group">
            <?php echo form_submit('submit','Guardar','class="btn btn-info"');?>
        </div>
        
        <?php 
        if(isset($edit) && isset($term_id)) {
            echo form_hidden('edit', TRUE);
            echo form_hidden('id', $id_receta);
        } 
        ?>
        
        
        <?php echo form_close();?>
    </div>
</div>