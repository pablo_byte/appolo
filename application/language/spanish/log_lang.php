<?php


// Log Messages

// Auth
$lang['login_successful']               = 'Usuario se conectó exitosamente';
$lang['login_unsuccessful']             = 'Inicio de sesión fallido';
$lang['logout_successful']              = 'Usuario desconectado exitosamente';
$lang['logout_unsuccessful']            = 'Error al desconectar usuario';

$lang['unauthorized_access']            = 'Acceso no autorizado';
$lang['not_logged']                     = 'Acceso sólo para usuarios que han iniciado sesión.';

$lang['user_access']                    = 'Acceso a lista de usuarios';
$lang['edit_user_access']               = 'Acceso a edición de usuario %s';
$lang['log_access']                     = 'Acceso a log de auditoría';

$lang['user_saved']                     = 'Usuario guardado correctamente';
$lang['activate_user_success']          = 'Usuario %s activado correctamente';
$lang['activate_user_fail']             = 'Usuario %s no se pudo activar';
$lang['deactivate_user_success']        = 'Usuario %s desactivado correctamente';
$lang['deactivate_user_fail']           = 'Usuario %s no se pudo desactivar';

// Term
$lang['term_access']                    = 'Acceso a lista de términos y condiciones';
$lang['term_add_access']                = 'Acceso a creación de nuevos términos y condiciones';
$lang['term_edit_access']               = 'Acceso a edición de términos y condiciones "%s"';
$lang['term_copy_access']               = 'Acceso a copia de términos y condiciones "%s"';

$lang['term_saved_success']             = 'Términos y condiciones "%s" guardado correctamente';
$lang['term_saved_fail']                = 'Términos y condiciones "%s" no se guardó corretamente';
$lang['term_update_success']            = 'Términos y condiciones "%s" actualizado correctamente';
$lang['term_update_fail']               = 'Términos y condiciones "%s" no se pudo actualizar';
$lang['term_copy_success']              = 'Términos y condiciones copiados con éxito desde "%s" a "%s"';
$lang['term_copy_fail']                 = 'No se pudo copiar términos y condiciones desde "%s" a "%s"';
$lang['term_deleted_success']           = 'Términos y condiciones "%s" borrado correctamente';
$lang['term_deleted_fail']              = 'Términos y condiciones "%s" no se pudo borrar';

// Versions
$lang['version_access']                 = 'Acceso a lista de versiones';
$lang['version_add_access']             = 'Acceso a creación de nueva versión';

$lang['version_saved_success']          = 'Versión %s para %s guardada correctamente';
$lang['version_saved_fail']             = 'Versión %s para %s no se guardó correctamente';
$lang['version_activate_success']       = 'Versión %s para %s activada correctamente';
$lang['version_activate_fail']          = 'Versión %s para %s no se pudo activar';
$lang['version_deactivate_success']     = 'Versión %s para %s desactivada correctamente';
$lang['version_deactivate_fail']        = 'Versión %s para %s no se pudo desactivar';
$lang['version_delete_success']         = 'Versión %s para %s borrada correctamente';
$lang['version_delete_fail']            = 'Versión %s para %s no se pudo borrar';

// Devices
$lang['device_types_access']            = 'Acceso a lista de tipos de dispositivos';

$lang['device_types_saved_success']         = 'Tipo de dispositivo %s guardado correctamente';
$lang['device_types_saved_fail']            = 'Tipo de dispositivo %s no se guardó correctamente';
$lang['device_types_update_success']        = 'Tipo de dispositivo %s actualizado correctamente';
$lang['device_types_update_fail']           = 'Tipo de dispositivo %s no se pudo editar';
$lang['device_types_activate_title']        = 'activar';
$lang['device_types_activate_success']      = 'Tipo de dispositivo %s activado correctamente';
$lang['device_types_activate_fail']         = 'Tipo de dispositivo %s no de pudo activar';
$lang['device_types_deactivate_title']      = 'desactivar';
$lang['device_types_deactivate_success']    = 'Tipo de dispositivo %s desactivado correctamente';
$lang['device_types_deactivate_fail']       = 'Tipo de dispositivs %s no se pudo desactivar';
$lang['device_types_delete_title']          = 'borrar';
$lang['device_types_delete_warning']        = '<b>Advertencia</b> Los dispositivos asociados a este tipo de dispositivo quedarán sin soporte hasta que sean reasignados';
$lang['device_types_delete_success']        = 'Tipo de dispositivo %s borrado correctamente';
$lang['device_types_delete_fail']           = 'Tipo de dispositivo %s no se pudo borrar';