<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Farmacos extends CI_Controller {

    var $data = array();

    function __construct() {

        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('navigation');

        $this->load->database();
        $this->load->model('farmaco');

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth', 'spanish');
        $this->lang->load('log', 'spanish');
        $this->load->helper('language');

        $this->data['title'] = 'Appolo :: Fármacos';
        $this->data['nav'] = TRUE;
        $this->data['user'] = $this->ion_auth->user($this->session->userdata('user_id'))->row();
    }

    function index() {
        if (!$this->ion_auth->logged_in()) {
            log_message('error', $this->lang->line('not_logged'));
            redirect('auth/login', 'refresh');
        } else if (!$this->ion_auth->is_admin()) {
            log_message('error', $this->lang->line('unauthorized_access'));
            return show_error('You must be an administrator to view this page.');
        }
        log_message('info', $this->lang->line('log_access'));

        $_levels = array(1 => 'ERROR', 2 => 'DEBUG', 3 => 'INFO', 4 => 'ALL');
        
        $this->data['date'] = array(
            'name' => 'daterange',
            'id' => 'date',
            'class' => 'form-control'
        );

        $this->load->view('templates/header', $this->data);
        $this->load->view('farmacos/index');
        $this->load->view('templates/footer');
    }

    function farmaco_table($start = 0, $end = 0, $level = 'all', $controller = 'all') {

        $columns = array(
            'id_farmaco',
            'codigo_farmaco',
            'nombre_farmaco',
            'tipo_farmaco',
            'estado_farmaco',
            'actions',
        );

        $order = $this->input->post('order');
        $limit = $this->input->post('start') . ',' . $this->input->post('length');
        $where = array(
            'start' => $start,
            'end' => $end,
            'level' => $level,
            'controller' => $controller
        );

        $farmacos = $this->farmaco->get($columns[$order[0]['column']], $order[0]['dir'], $limit, $where);
        $total = $this->farmaco->count();
        $filtered = $this->farmaco->count($where);

        $output = array(
            'draw' => (int) $this->input->post('draw'),
            'recordsTotal' => $total,
            'recordsFiltered' => $filtered,
            'data' => array()
        );

        if (count($farmacos) > 0) {
            foreach ($farmacos as $farmaco) {
                $row = array();
                $active = (bool) $farmaco->estado_farmaco;
                for ($i = 0; $i < count($columns); $i++) {
                    //$row[] = $log->$columns[$i];
                    switch ($columns[$i]) {
                        case 'estado_farmaco':
                            if($active) {
                                $row[] = '<span class="label label-success">Activo</span>';
                            } else {
                                $row[] = '<span class="label label-danger">Inactivo</span>';
                            }
                            break;
                            
                        case 'actions':
                            $btn_group = '<div class="btn-group" role="group">';
                            $btn_group .= '<button class="btn btn-default btn-sm" onclick="edit_farmaco('.$farmaco->id_farmaco.')">Editar</button>';
                            
                            if($active) {
                                $btn_group .= '<button class="btn btn-warning btn-sm" onclick="action_modal('.$farmaco->id_farmaco.', \'farmacos/action_farmaco\', \'deactivate\')">Desactivar</button>';
                            } else {
                                $btn_group .= '<button class="btn btn-success btn-sm" onclick="action_modal('.$farmaco->id_farmaco.', \'farmacos/action_farmaco\', \'activate\')">Activar</button>';
                            }
                            
                            //$btn_group .= '<button class="btn btn-danger btn-sm" onclick="action_modal('.$farmaco->id_farmaco.', \'farmacos/action_farmaco\', \'delete\')">Borrar</button>';
                            $btn_group .= '</div>';
                            
                            $row[] = $btn_group;
                            break;
                            
                        default:
                            $row[] = $farmaco->$columns[$i];
                            break;
                    }
                }
                $output['data'][] = $row;
            }
        }

        echo json_encode($output);
    }

    public function save() {
        header('Content-type: application/json');
        $output = array();
        $message = '';
        $status = 'error';

        $this->form_validation->set_rules('codigo_farmaco', 'Código', 'required|xss_clean');
        $this->form_validation->set_rules('nombre_farmaco', 'Nombre', 'require|xss_clean');
        $this->form_validation->set_rules('tipo_administracion', 'Tipo', 'require|xss_clean');

        if ($this->form_validation->run()) {
            $save = $this->input->post('save_type');

            $data = array(
                'codigo_farmaco' => $this->input->post('codigo_farmaco'),
                'nombre_farmaco' => $this->input->post('nombre_farmaco'),
                'tipo_administracion' => $this->input->post('tipo_administracion'),
            );
            print_r($data);die();
            
            if ($save == 'add') {
                if ($this->farmaco->insert_farmaco($data)) {
                    $message = sprintf($this->lang->line('device_types_saved_success'), $data['name']);
                    log_message('info', $message);
                    $status = 'success';
                } else {
                    $message = sprintf($this->lang->line('device_type_saved_fail'), $data['name']);
                    log_message('error', $message);
                }
                
            } else if ($save == 'edit') {
                $id = $this->input->post('type_id');

                if ($this->farmaco->update_farmaco($data, array('id' => $id))) {
                    $message = sprintf($this->lang->line('device_types_update_success'), $data['name']);
                    log_message('info', $message);
                    $status = 'success';
                } else {
                    $message = sprintf($this->lang->line('device_types_update_fail'), $data['name']);
                }
            }
        }

        $output['message'] = (validation_errors()) ? validation_errors() : $message;
        $output['status'] = $status;

        echo json_encode($output);
    }
    
    public function get_farmaco($id){
        header('Content-type: application/json');
        
        $type = $this->farmaco->get_farmaco($id);
        
        echo json_encode($type[0]);
        
    }
    
    public function action_farmaco() {
        header('Content-type: application/json');
        
        $id = $this->input->post('id');
        $status = (bool) $this->input->post('status');
        $action = $this->input->post('action');
        $farmaco = $this->farmaco->get_farmaco($id);
        
        $output = array(
            'action'    => array(
                'action'    => $action,
                'title'     => $this->lang->line('device_types_'.$action.'_title'),
                'url'       => 'farmacos/action_farmaco'
            ),
            'obj'       => array(
                'title' => 'Nombre de Fármaco: '.$farmaco[0]->nombre_farmaco,
                'id'    => $farmaco[0]->id
            ),
            'table'     => '#farmacos-table'
        );
        
        if($action == 'delete') {
            $output['warning'] = $this->lang->line('device_types_delete_warning');
        }
        
        if($status) {
            switch($action) {
                case 'activate':
                    $data = array('estado_farmaco' => 1);
                    $return = $this->farmaco->update_farmaco($data, array('id' => $id));
                    break;
                
                case 'deactivate':
                    $data = array('estado_farmaco' => 0);
                    $return = $this->farmaco->update_farmaco($data, array('id' => $id));
                    break;
                
                /*case 'delete':
                    $return = $this->farmaco->delete_farmaco($id);
                    break;*/
                
            }
            
            if($return) {
                $message = sprintf($this->lang->line('device_types_'.$action.'_success'), $farmaco[0]->nombre_farmaco);
                log_message('info', $message);
                $output['status'] = 'success';
                $output['message'] = $message;
                
            } else {
                $message = sprintf($this->lang->line('device_types_'.$action.'_fail'), $farmaco[0]->nombre_farmaco);
                log_message('error', $message);
                $output['status'] = 'error';
                $output['error'] = $message;
            }
        }
        
        echo json_encode($output);
    }

}
