<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Recetas extends CI_Controller {

    var $data = array();

    function __construct() {

        parent::__construct();
        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('navigation');
        $this->load->helper('ckeditor');

        $this->load->database();
        $this->load->model('receta');

        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

        $this->lang->load('auth', 'spanish');
        $this->lang->load('log', 'spanish');
        $this->load->helper('language', 'spanish');

        $this->load->config('appolo', TRUE);

        $this->data['title'] = 'Appolo :: Recetas';
        $this->data['nav'] = TRUE;
        $this->data['user'] = $this->ion_auth->user($this->session->userdata('user_id'))->row();
        $this->data['tipo_user'] = $this->ion_auth->user($this->session->userdata('tipo'))->row();
        $this->data['groups'] = $this->ion_auth->group($this->data['user']->id)->result_array();
    }

    function index() {
        if (!$this->ion_auth->logged_in()) {
            log_message('error', $this->lang->line('not_logged'));
            redirect('auth/login', 'refresh');
        } else {

            //print_r($this->data['user']);die();
            log_message('info', $this->lang->line('log_access'));

            $_levels = array(1 => 'ERROR', 2 => 'DEBUG', 3 => 'INFO', 4 => 'ALL');

            $this->data['user_id'] = $this->data['user']->id;
            //$this->data['user_type'] = $this->data['groups'][0]['id'];

            $this->data['date'] = array(
                'name' => 'daterange',
                'id' => 'date',
                'class' => 'form-control'
            );

            $this->load->view('templates/header', $this->data);
            $this->load->view('recetas/index');
            $this->load->view('templates/footer');
        }
    }

    function receta_table($start = 0, $end = 0, $level = 'all', $controller = 'all') {

        $this->data['user_id'] = $this->data['user']->id;
        $this->data['user_rut'] = $this->data['user']->rut;
        $this->data['tipo_user'] = $this->data['user']->tipo;

        $columns = array(
            'id_receta',
            'tipo_receta',
            'rut_doctor',
            'rut_paciente',
            'nombre_paciente',
            'created',
            'estado_receta',
            'actions',
        );

        $order = $this->input->post('order');
        $limit = $this->input->post('start') . ',' . $this->input->post('length');
        if ($this->ion_auth->is_admin()) {
            $where = array(
                //'rut_doctor' => $this->data['user_rut'],
                'start' => $start,
                'end' => $end,
            );
        } else if ($this->data['tipo_user'] == 2) {
            $where = array(
                'rut_doctor' => $this->data['user_rut'],
                'start' => $start,
                'end' => $end,
            );
        }

        $recetas = $this->receta->get($columns[$order[0]['column']], $order[0]['dir'], $limit, $where);
        $total = $this->receta->count();
        $filtered = $this->receta->count($where);

        $output = array(
            'draw' => (int) $this->input->post('draw'),
            'recordsTotal' => $total,
            'recordsFiltered' => $filtered,
            'data' => array()
        );
        
        $tipo_recetas = $this->config->item('tipo_recetas', 'appolo');
        
        if (count($recetas) > 0) {
            foreach ($recetas as $receta) {
                $row = array();
                $active = (bool) $receta->estado_receta;
                for ($i = 0; $i < count($columns); $i++) {
                    //$row[] = $log->$columns[$i];
                    switch ($columns[$i]) {
                        
                        case 'tipo_receta':
                            $row[] = $tipo_recetas[$receta->tipo_receta];
                            break;
                        
                        case 'estado_receta':
                            if ($active) {
                                $row[] = '<span class="label label-success">Activo</span>';
                            } else {
                                $row[] = '<span class="label label-danger">Inactivo</span>';
                            }
                            break;

                        case 'actions':
                            $btn_group = '<div class="btn-group" role="group">';
                            $btn_group .= '<a class="btn btn-info btn-sm" href="' . base_url() . 'recetas/detalle_receta/' . $receta->id_receta . '">Ver</a>';
                            $btn_group .= '</div>';

                            $row[] = $btn_group;
                            break;

                        default:
                            $row[] = $receta->$columns[$i];
                            break;
                    }
                }
                $output['data'][] = $row;
            }
        }

        echo json_encode($output);
    }

    public function save() {
        header('Content-type: application/json');
        $output = array();
        $message = '';
        $status = 'error';

        $this->form_validation->set_rules('codigo_farmaco', 'Código', 'required|xss_clean');
        $this->form_validation->set_rules('nombre_farmaco', 'Nombre', 'require|xss_clean');
        $this->form_validation->set_rules('tipo_administracion', 'Tipo', 'require|xss_clean');

        if ($this->form_validation->run()) {
            $save = $this->input->post('save_type');

            $data = array(
                'codigo_farmaco' => $this->input->post('codigo_farmaco'),
                'nombre_farmaco' => $this->input->post('nombre_farmaco'),
                'tipo_administracion' => $this->input->post('tipo_administracion'),
            );
            //print_r($data);die();

            if ($save == 'add') {
                if ($this->farmaco->insert_farmaco($data)) {
                    $message = sprintf($this->lang->line('device_types_saved_success'), $data['name']);
                    log_message('info', $message);
                    $status = 'success';
                } else {
                    $message = sprintf($this->lang->line('device_type_saved_fail'), $data['name']);
                    log_message('error', $message);
                }
            } else if ($save == 'edit') {
                $id = $this->input->post('type_id');

                if ($this->farmaco->update_farmaco($data, array('id' => $id))) {
                    $message = sprintf($this->lang->line('device_types_update_success'), $data['name']);
                    log_message('info', $message);
                    $status = 'success';
                } else {
                    $message = sprintf($this->lang->line('device_types_update_fail'), $data['name']);
                }
            }
        }

        $output['message'] = (validation_errors()) ? validation_errors() : $message;
        $output['status'] = $status;

        echo json_encode($output);
    }

    public function get_receta($id) {
        header('Content-type: application/json');

        $type = $this->farmaco->get_farmaco($id);

        echo json_encode($type[0]);
    }

    public function action_farmaco() {
        header('Content-type: application/json');

        $id = $this->input->post('id');
        $status = (bool) $this->input->post('status');
        $action = $this->input->post('action');
        $farmaco = $this->farmaco->get_farmaco($id);

        $output = array(
            'action' => array(
                'action' => $action,
                'title' => $this->lang->line('device_types_' . $action . '_title'),
                'url' => 'farmacos/action_farmaco'
            ),
            'obj' => array(
                'title' => 'Nombre de Fármaco: ' . $farmaco[0]->nombre_farmaco,
                'id' => $farmaco[0]->id
            ),
            'table' => '#farmacos-table'
        );

        if ($action == 'delete') {
            $output['warning'] = $this->lang->line('device_types_delete_warning');
        }

        if ($status) {
            switch ($action) {
                case 'activate':
                    $data = array('estado_farmaco' => 1);
                    $return = $this->farmaco->update_farmaco($data, array('id' => $id));
                    break;

                case 'deactivate':
                    $data = array('estado_farmaco' => 0);
                    $return = $this->farmaco->update_farmaco($data, array('id' => $id));
                    break;

                /* case 'delete':
                  $return = $this->farmaco->delete_farmaco($id);
                  break; */
            }

            if ($return) {
                $message = sprintf($this->lang->line('device_types_' . $action . '_success'), $farmaco[0]->nombre_farmaco);
                log_message('info', $message);
                $output['status'] = 'success';
                $output['message'] = $message;
            } else {
                $message = sprintf($this->lang->line('device_types_' . $action . '_fail'), $farmaco[0]->nombre_farmaco);
                log_message('error', $message);
                $output['status'] = 'error';
                $output['error'] = $message;
            }
        }

        echo json_encode($output);
    }

    public function detalle_receta($id_receta = null) {

        if (!$id_receta) {
            return show_error('Error al Buscar Detalle de Receta');
        }

        if (!$this->ion_auth->logged_in()) {
            log_message('error', $this->lang->line('not_logged'));
            redirect('auth/login', 'refresh');
        } /*else if (!$this->ion_auth->is_admin()) {
            log_message('error', $this->lang->line('unauthorized_access'));
            return show_error('You must be an administrator to view this page.');
        }*/
        log_message('info', $this->lang->line('log_access'));

        $this->data['id_receta'] = $id_receta;
        $this->data['data_receta'] = $this->receta->get_receta($id_receta);
        
        $this->data['ckeditor'] = array(
            'id' => 'detalle_receta',
            'path' => 'assets/js/plugins/ckeditor',
            'isReadOnly' => true,
        );
        
        $this->load->view('templates/header', $this->data);
        $this->load->view('recetas/detalle_receta');
        $this->load->view('templates/footer');
    }

    function detalle_receta_table($id_receta) {

        $columns = array(
            'id',
            'id_farmaco',
            'nombre_farmaco',
            'tipo_farmaco',
            'dosis',
            'cant_total',
            'estado_item',
            'actions',
        );

        $order = $this->input->post('order');
        //$limit = $this->input->post('start') . ',' . $this->input->post('length');
        
        $items_receta = $this->receta->get_items($id_receta/*, $order[0]['dir'], $limit, $where*/);
        $total = $this->receta->count();
        $filtered = $this->receta->count($items_receta);

        $output = array(
            'draw' => (int) $this->input->post('draw'),
            'recordsTotal' => $total,
            'recordsFiltered' => $filtered,
            'data' => array()
        );

        if (count($items_receta) > 0) {
            foreach ($items_receta as $item) {
                $row = array();
                $active = (bool) $item->estado_item;
                for ($i = 0; $i < count($columns); $i++) {
                    switch ($columns[$i]) {
                        case 'tipo':
                            if ($active) {
                                $row[] = '<span class="label label-success">Activo</span>';
                            } else {
                                $row[] = '<span class="label label-danger">Inactivo</span>';
                            }
                            break;
                        case 'estado_item':
                            if ($active) {
                                $row[] = '<span class="label label-success">Activo</span>';
                            } else {
                                $row[] = '<span class="label label-danger">Inactivo</span>';
                            }
                            break;

                        case 'actions':
                            $btn_group = '<div class="btn-group" role="group">';
                            $btn_group .= '<a class="btn btn-info btn-sm" href="' . base_url() . 'recetas/detalle_farmaco/' . $item->id_farmaco . '">Ver</a>';
                            $btn_group .= '</div>';

                            $row[] = $btn_group;
                            break;

                        default:
                            $row[] = $item->$columns[$i];
                            break;
                    }
                }
                $output['data'][] = $row;
            }
        }

        echo json_encode($output);
    }

    public function new_receta($edit = FALSE, $id = FALSE) {
        if (!$this->ion_auth->logged_in()) {
            log_message('error', $this->lang->line('not_logged'));
            redirect('auth/login', 'refresh');
        } else /* if(!$this->ion_auth->is_admin()) */ {
            //log_message('error',$this->lang->line('unauthorized_access'));
            //return show_error('You must be an administrator to view this page.');
            //}

            $this->form_validation->set_rules('tipo_receta', 'Tipo Receta', 'required|xss_clean');
            $this->form_validation->set_rules('rut_paciente', 'Rut Paciente', 'required|xss_clean');
            $this->form_validation->set_rules('nombre_paciente', 'Nombre Paciente', 'required|xss_clean');
            $this->form_validation->set_rules('edad_paciente', 'Edad Paciente', 'required|numeric|xss_clean');
            $this->form_validation->set_rules('direccion_paciente', 'dirección Paciente', 'required|xss_clean');
            $this->form_validation->set_rules('detalle_receta', 'Detalle Receta', 'required|xss_clean');

            if ($this->form_validation->run() == TRUE) {
                $data = array(
                    'tipo_receta'       => $this->input->post('tipo_receta'),
                    'rut_doctor'        => $this->data['user']->rut,
                    'rut_paciente'      => $this->input->post('rut_paciente'),
                    'nombre_paciente'   => $this->input->post('nombre_paciente'),
                    'edad_paciente'     => $this->input->post('edad_paciente'),
                    'direccion_paciente' => $this->input->post('direccion_paciente'),
                    'detalle_receta'    => $this->input->post('detalle_receta'),
                );
                //print_r($data);die();

                if ($this->input->post('edit') && $this->input->post('id_receta')) {
                    $data['updated'] = date('Y-m-d H:i:s');

                    if ($this->terms->update($data, array('id' => $this->input->post('id')))) {
                        $flash_message = sprintf($this->lang->line('term_update_success'), $data['title']);
                        log_message('info', $flash_message);
                        $this->session->set_flashdata('message', $flash_message);
                        redirect('/recetas', 'refresh');
                    } else {
                        $error = sprintf($this->lang->line('term_update_fail'), $data['title']);
                        log_message('error', $error);
                        $this->session->set_flashdata('message', $error);
                    }
                } else {

                    $data['created'] = date('Y-m-d H:i:s');
                    if ($this->receta->insert_it($data)) {
                        $flash_message = sprintf($this->lang->line('term_saved_success'), $data['title']);
                        log_message('info', $flash_message);
                        $this->session->set_flashdata('message', $flash_message);
                        redirect('/recetas', 'refresh');
                    } else {
                        $error = sprintf($this->lang->line('term_saved_fail'), $data['title']);
                        log_message('error', $error);
                        $this->session->set_flashdata('message', $error);
                    }
                }
            }

            /* if ($edit) {

              $term = $this->terms->get($id);
              log_message('info', sprintf($this->lang->line('term_edit_access'), $term->title));
              $this->data['header_title'] = 'Editar: ' . $term->title;
              $this->data['edit'] = TRUE;
              $this->data['term_id'] = $id;
              $countries = FALSE;

              $term_title = $term->title;
              $term_text = $term->text;
              $term_code = $term->country;
              } else { */

            log_message('info', $this->lang->line('term_add_access'));

            $this->data['header_title'] = 'Nueva Receta';
            $tipo_recetas = $this->config->item('tipo_recetas', 'appolo');
            //print_r($tipo_recetas);die();
            //$countries = $this->terms->getCountries();

            //$term_title = '';
            //$term_text = '';
            //$term_code = '';
            $tipo_receta_options = '';
            $rut_paciente = '';
            $nombre_paciente = '';
            $edad_paciente = '';
            $direccion_paciente = '';
            $detalle_receta = '';
            //}


            $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

            $this->data['tipo_receta_options'] = array('' => 'Seleccionar') + $tipo_recetas; //get_countries($countries);
            $this->data['tipo_receta__options_selected'] = $this->form_validation->set_value('tipo_receta_options') ? $this->form_validation->set_value('tipo_receta_options') : $tipo_receta_options;
            $this->data['fecha'] = date("d/m/Y");

            $this->data['rut_paciente'] = array(
                'name' => 'rut_paciente',
                'id' => 'rut_paciente',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('rut_paciente') ? $this->form_validation->set_value('rut_paciente') : $rut_paciente
            );

            $this->data['nombre_paciente'] = array(
                'name' => 'nombre_paciente',
                'id' => 'nombre_paciente',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('nombre_paciente') ? $this->form_validation->set_value('nombre_paciente') : $nombre_paciente
            );

            $this->data['edad_paciente'] = array(
                'name' => 'edad_paciente',
                'id' => 'edad_paciente',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('edad_paciente') ? $this->form_validation->set_value('edad_paciente') : $edad_paciente
            );

            $this->data['direccion_paciente'] = array(
                'name' => 'direccion_paciente',
                'id' => 'direccion_paciente',
                'type' => 'text',
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('direccion_paciente') ? $this->form_validation->set_value('direccion_paciente') : $direccion_paciente
            );

            $this->data['detalle_receta'] = array(
                'name' => 'detalle_receta',
                'id' => 'detalle_receta',
                'cols' => 15,
                'rows' => 15,
                'class' => 'form-control',
                'value' => $this->form_validation->set_value('detalle_receta') ? $this->form_validation->set_value('detalle_receta') : $detalle_receta
            );

            $this->data['ckeditor'] = array(
                'id' => 'detalle_receta',
                'path' => 'assets/js/plugins/ckeditor'
            );

            $this->load->view('templates/header', $this->data);
            $this->load->view('recetas/form');
            $this->load->view('templates/footer');
        }
    }

}
