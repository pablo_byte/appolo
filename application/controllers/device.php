<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Device extends CI_Controller {
    
    public $data = array();
    public function __construct()
    {
        parent::__construct();
        
        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        
        $this->load->helper('url');
        $this->load->helper('navigation');
        $this->load->helper('language');
        
        $this->load->model('devices');
        
        $this->load->config('skynouk', TRUE);
        
        $this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));
        
        $this->lang->load('auth');
        
        $this->data['nav'] = TRUE;
        $this->data['title'] = 'Skynouk';
        $this->data['user'] = $this->ion_auth->user($this->session->userdata('user_id'))->row();
        
        if(!$this->ion_auth->logged_in()) {
            log_message('error',$this->lang->line('not_logged'));
            redirect('auth/login','refresh');
        } else if(!$this->ion_auth->is_admin()) {
            log_message('error',$this->lang->line('unauthorized_access'));
            return show_error('You must be an administrator to view this page.');
        }
    }
    
    public function index()
    {
        $this->load->view('templates/header', $this->data);
        $this->load->view('devices/index');
        $this->load->view('templates/footer');
    }
    
    public function device_types_table()
    {
        $columns = array(
            'name',
            'count',
            'active',
            'actions'
        );
        
        $order = $this->input->post('order');
        $limit = $this->input->post('start').','.$this->input->post('length');
        
        $types = $this->devices->get_type(NULL, $columns[$order[0]['column']], $order[0]['dir'], $limit);
        $recordsTotal = $recordsFiltered = $this->devices->count_type();
        
        $output = array(
            'draw'  => (int) $this->input->post('draw'),
            'recordsTotal' => $recordsTotal,
            'recordsFiltered' => $recordsFiltered,
            'data' => array()
        );
        
        if($types)
        {
            foreach($types as $type)
            {
                $row = array();
                $active = (bool) $type->active;
                for($i=0;$i<count($columns);$i++)
                {
                    switch ($columns[$i])
                    {
                        case 'active':
                            if($active)
                            {
                                $row[] = '<span class="label label-success">Activo</span>';
                            } else {
                                $row[] = '<span class="label label-danger">Inactivo</span>';
                            }
                            break;
                        case 'actions':
                            $btn_group = '<div class="btn-group" role="group">';
                            $btn_group .= '<button class="btn btn-default" onclick="edit_type_device('.$type->id.')">Editar</button>';
                            
                            if($active) {
                                $btn_group .= '<button class="btn btn-warning" onclick="action_modal('.$type->id.', \'device/action_type\', \'deactivate\')">Desactivar</button>';
                            } else {
                                $btn_group .= '<button class="btn btn-success" onclick="action_modal('.$type->id.', \'device/action_type\', \'activate\')">Activar</button>';
                            }
                            
                            $btn_group .= '<button class="btn btn-danger" onclick="action_modal('.$type->id.', \'device/action_type\', \'delete\')">Borrar</button>';
                            $btn_group .= '</div>';
                            
                            $row[] = $btn_group;
                            break;
                        default:
                            $row[] = $type->$columns[$i];
                            break;
                    }
                }
                $output['data'][] = $row;
             }
        }
        
        echo json_encode($output);
    }
    
    public function get_device_type($id)
    {
        header('Content-type: application/json');
        
        $type = $this->devices->get_type($id);
        
        echo json_encode($type[0]);
        
    }
    
    public function save_type()
    {
        header('Content-type: application/json');
        $output = array();
        $message = '';
        $status = 'error';
        
        $this->form_validation->set_rules('type_name', 'Nombre', 'required|xss_clean');
        $this->form_validation->set_rules('type_geo', 'Georeferencia', 'require|xss_clean');
        $this->form_validation->set_rules('type_img', 'Imagen', 'require|xss_clean');
        $this->form_validation->set_rules('type_vid', 'Video', 'required|xss_clean');
        
        if($this->form_validation->run())
        {
            $save = $this->input->post('save_type');
            
            $data = array(
                'name'          => $this->input->post('type_name'),
                'georeference'  => $this->input->post('type_geo'),
                'image'         => $this->input->post('type_img'),
                'video'         => $this->input->post('type_vid')
            );
            
            if($save == 'add')
            {
                if($this->devices->insert_type($data))
                {
                    $message = sprintf($this->lang->line('device_types_saved_success'),$data['name']);
                    log_message('info', $message);
                    $status = 'success';
                } else {
                    $message = sprintf($this->lang->line('device_type_saved_fail'),$data['name']);
                    log_message('error', $message);
                }
            } else if($save == 'edit')
            {
                $id = $this->input->post('type_id');
                
                if($this->devices->update_type($data,array('id' => $id)))
                {
                    $message = sprintf($this->lang->line('device_types_update_success'),$data['name']);
                    log_message('info', $message);
                    $status = 'success';
                } else {
                    $message = sprintf($this->lang->line('device_types_update_fail'), $data['name']);
                }
            }
        }
        
        $output['message'] = (validation_errors()) ? validation_errors() : $message;
        $output['status'] = $status;
        
        echo json_encode($output);
    }
    
    public function action_type()
    {
        header('Content-type: application/json');
        
        $type_id = $this->input->post('id');
        $status = (bool) $this->input->post('status');
        $action = $this->input->post('action');
        $type = $this->devices->get_type($type_id);
        
        $output = array(
            'action'    => array(
                'action'    => $action,
                'title'     => $this->lang->line('device_types_'.$action.'_title'),
                'url'       => 'device/action_type'
            ),
            'obj'       => array(
                'title' => 'Tipo de dispositivo: '.$type[0]->name,
                'id'    => $type[0]->id
            ),
            'table'     => '#device-types-table'
        );
        
        if($action == 'delete')
        {
            $output['warning'] = $this->lang->line('device_types_delete_warning');
        }
        
        if($status)
        {
            switch($action)
            {
                case 'activate':
                    $data = array('active' => 1);
                    $return = $this->devices->update_type($data, array('id' => $type_id));
                    break;
                case 'deactivate':
                    $data = array('active' => 0);
                    $return = $this->devices->update_type($data, array('id' => $type_id));
                    break;
                case 'delete':
                    $return = $this->devices->delete_type($type_id);
                    break;
            }
            
            if($return)
            {
                $message = sprintf($this->lang->line('device_types_'.$action.'_success'), $type[0]->name);
                log_message('info', $message);
                $output['status'] = 'success';
                $output['message'] = $message;
            } else {
                $message = sprintf($this->lang->line('device_types_'.$action.'_fail'), $type[0]->name);
                log_message('error', $message);
                $output['status'] = 'error';
                $output['error'] = $message;
            }
        }
        
        echo json_encode($output);
    }
    
    
    // Events
    
    public function events()
    {
        $this->load->view('templates/header', $this->data);
        $this->load->view('devices/events_index');
        $this->load->view('templates/footer');
    }
    
}